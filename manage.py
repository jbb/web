#!/usr/bin/env python
from flask import current_app
from flask.ext.script import Manager, Server, Shell, Option, prompt_bool

from accounts import create_app
from accounts.forms import RegisterForm
from accounts.utils.console import Command, ConsoleForm, TablePrinter
from accounts.backend.mail.dummy import DummyBackend as DummyMailBackend


class ListServices(Command):
    """List the configured services."""
    name = 'list-services'

    def run(self):
        table = TablePrinter(['Name', 'URL'])
        table.output([(service.name, service.url)
                      for service in current_app.all_services])


class ListUsers(Command):
    """List registered users."""
    name = 'list-users'

    option_list = (
        Option('--locked', '-l', action='store_true', dest='locked',
               help='Include locked users.'),
        Option('--only-locked', '-L', action='store_true', dest='only_locked',
               help='Show ONLY locked users.'),
    )

    def _get_users(self, locked, only_locked):
        for user in current_app.user_backend.find():
            user_locked = user.mail.startswith('noreply-disabledaccount-')
            if user_locked and not (locked or only_locked):
                continue
            elif not user_locked and only_locked:
                continue

            yield user

    def run(self, locked, only_locked):
        table = TablePrinter(['Name', 'E-Mail', 'Uid'])
        table.output([(user.uid, user.mail, user.uidNumber)
                      for user in self._get_users(locked, only_locked)])


class CreateUser(Command):
    """Register a user."""
    name = 'create-user'

    option_list = (
        Option('--ignore-blacklist', action='store_true',
               dest='ignore_blacklist', help='Ignore blackisted user names.'),
        Option('--print', '-p', action='store_true', dest='print_only',
               help='Do not send the activation link via mail, only print it.'),
        Option(dest='username', metavar='USERNAME',
               help='Username that should be registered.'),
        Option(dest='mail', metavar='MAIL',
               help='Mail address of the new user.'),
    )

    def run(self, username, mail, ignore_blacklist=False, print_only=False):
        form = ConsoleForm(RegisterForm, username=username, mail=mail)
        form.csrf_enabled = False
        del form.question

        if ignore_blacklist and prompt_bool('Blacklist wirklich ignorieren?'):
            current_app.username_blacklist = []

        if not form.validate():
            form.print_errors()
            return

        if print_only:
            current_app.mail_backend = DummyMailBackend(current_app)

        current_app.mail_backend.send(mail, 'mail/register.txt',
                                      username=username)

        if not print_only:
            print('Mail versandt.')


def main():
    manager = Manager(create_app)
    manager.add_option('-c', '--config', dest='config', required=False)

    manager.add_command(
        'runserver', Server(host='::', use_debugger=False))
    manager.add_command(
        'debug', Server(host='::', use_debugger=True))
    manager.add_command(
        'shell', Shell())
    manager.add_command(ListServices)
    manager.add_command(ListUsers)
    manager.add_command(CreateUser)
    manager.run()


if __name__ == '__main__':
    main()
