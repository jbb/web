#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import ldap
from os.path import dirname, abspath
sys.path.append(dirname(dirname(abspath(__file__))))

from account import AccountService
from app import app

"""
  Create a new service

  With this script you can easily create a new service entry in the ldap
  backend.

  Usage:
    . env/bin/activate
    contrib/create_service.py name_of_new_service
"""

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: %s name" % sys.argv[0])
        exit(-1)

    name = sys.argv[1]
    service = AccountService(app.config['LDAP_HOST'], app.config['LDAP_BASE_DN'],
        app.config['LDAP_ADMIN_USER'], app.config['LDAP_ADMIN_PASS'],
        app.all_services)


    service._bind_as_admin()

    dn = service._format_dn([('cn',name),('ou','services')])

    try:
      data = service.connection.search_s(dn, ldap.SCOPE_SUBTREE)
      print("'%s' already exists as service." % name)

    except ldap.NO_SUCH_OBJECT:
        attr =  [
            ('objectClass', ['top','organizationalRole']),
            ('cn', service._escape(name))
        ]

        service.connection.add_s(dn, attr)
        print("Successfully created '%s'  as a new service." % name)

    service._unbind()
