#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from os.path import dirname, abspath
sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

from account import AccountService
from app import app

if __name__ == "__main__":

    if len(sys.argv) > 1 and sys.argv[1] == 'config':
        print('graph_title Spline Accounts')
        print('graph_vlabel Number')
        print('accounts.label Accounts')
    else:
        service = AccountService(app.config['LDAP_HOST'], app.config['LDAP_BASE_DN'],
            app.config['LDAP_ADMIN_USER'], app.config['LDAP_ADMIN_PASS'],
            app.all_services)

        accounts = service.find()

        print('accounts.value %d' % len(accounts))
