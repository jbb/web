# -*- coding: utf-8 -*-
import os
from flask import Flask, g, session

from models import Service
from utils import get_backend
from utils.confirmation import Confirmation
from utils.sessions import EncryptedSessionInterface
from utils.login import create_login_manager
from views import default, login, admin


def absolute_paths(app, config):
    def handle_option(dirname, name):
        if app.config.get(name):
            app.config[name] = os.path.join(dirname, app.config[name])

    dirname = os.path.dirname(config)
    handle_option(dirname, 'USERNAME_BLACKLIST_FILE')


def load_config(app, configfile):
    if configfile is not None:
        filename = os.path.abspath(configfile)
        app.config.from_pyfile(filename)
        absolute_paths(app, filename)


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_object('accounts.default_settings')
    load_config(app, os.environ.get('SPLINE_ACCOUNT_WEB_SETTINGS'))
    load_config(app, config)

    app.register_blueprint(default.bp)
    app.register_blueprint(login.bp)
    app.register_blueprint(admin.bp, url_prefix='/admin')
    app.session_interface = EncryptedSessionInterface()

    app.all_services = list()
    for (name, url) in app.config.get('SERVICES', list()):
        cn = name.lower()
        app.all_services.append(Service(cn, name, url))

    app.username_blacklist = list()
    if app.config.get('USERNAME_BLACKLIST_FILE'):
        with open(app.config['USERNAME_BLACKLIST_FILE']) as f:
            app.username_blacklist = [line.rstrip() for line in f]

    login_manager = create_login_manager()
    login_manager.init_app(app)

    app.jinja_env.globals.update(
        confirm=lambda realm, *args: Confirmation(realm).dumps(tuple(args)))

    app.user_backend = get_backend(app.config['USER_BACKEND'], app)
    app.mail_backend = get_backend(app.config['MAIL_BACKEND'], app)

    return app
