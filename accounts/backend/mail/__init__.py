# -*- coding: utf-8 -*-

class Backend(object):

    def __init__(self, app):
        self.app = app

    def _send(self, recipient, content):
        raise NotImplementedError()

    def send(self, recipient, template, **kwargs):
        if recipient is None:
            return

        tmpl = self.app.jinja_env.get_or_select_template(template)

        kwargs['recipient'] = recipient
        module = tmpl.make_module(kwargs)

        self._send(recipient, module)
