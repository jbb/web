# -*- coding: utf-8 -*-
from __future__ import absolute_import

from . import Backend
from accounts.utils import ensure_utf8


FANCY_FORMAT = '''\
,---------------------------------------------------------------------------
| Subject: {subject}
| To: {to}
| From: {sender}
|---------------------------------------------------------------------------
| {body}
`---------------------------------------------------------------------------'''


PLAIN_FORMAT = '''Subject: {subject}
To: {to}
From: {sender}

{body}'''


class DummyBackend(Backend):

    def __init__(self, app, plain=False, format=None):
        super(DummyBackend, self).__init__(app)
        self.plain = plain

        if format is None:
            if self.plain:
                self.format = PLAIN_FORMAT
            else:
                self.format = FANCY_FORMAT
        else:
            self.format = format

    def _send(self, recipient, content):
        body = content.body()
        if not self.plain:
            body = "\n| ".join(body.split("\n"))

        print(self.format.format(
            subject=ensure_utf8(content.subject()),
            sender=ensure_utf8(content.sender()),
            to=ensure_utf8(recipient),
            body=ensure_utf8(body)))
