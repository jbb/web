# -*- coding: utf-8 -*-
from __future__ import absolute_import

from flask import Blueprint
from flask import current_app, redirect, request, g, flash, url_for
from flask.ext.login import current_user
from uuid import uuid4
from werkzeug.exceptions import Forbidden

from accounts.utils import templated
from accounts.forms import AdminCreateAccountForm, AdminDisableAccountForm


bp = Blueprint('admin', __name__)


@bp.before_request
def restrict_bp_to_admins():
    if not current_user.is_authenticated():
        return current_app.login_manager.unauthorized()
    if current_user.uid not in current_app.config.get('ADMIN_USERS', []):
        raise Forbidden(u'Du bist kein Admin.')


@bp.route('/')
@templated('admin/index.html')
def index():
    return {}


@bp.route('/create_account', methods=['GET', 'POST'])
@templated('admin/create_account.html')
def create_account():
    form = AdminCreateAccountForm()
    if form.validate_on_submit():
        current_app.mail_backend.send(form.mail.data, 'mail/register.txt',
                                      username=form.username.data)

        flash(u'Mail versandt.', 'success')
        return redirect(url_for('admin.index'))
    return {'form': form}


@bp.route('/view_blacklist')
@bp.route('/view_blacklist/<start>')
@templated('admin/view_blacklist.html')
def view_blacklist(start=''):
    entries = current_app.username_blacklist
    if start:
        entries = [e for e in entries if e.startswith(start)]

    next_letters = set(e[len(start)] for e in entries if len(e) > len(start))

    return {
        'entries': entries,
        'start': start,
        'next_letters': next_letters,
    }


@bp.route('/disable_account', methods=['GET', 'POST'])
@templated('admin/disable_account.html')
def disable_account():
    form = AdminDisableAccountForm()
    if 'uid' in request.args:
        form = AdminDisableAccountForm(username=request.args['uid'])

    if form.validate_on_submit():
        random_pw = str(uuid4())
        form.user.change_password(random_pw)
        for service in current_app.all_services:
            form.user.reset_password(service.id)

        oldmail = form.user.mail
        mail = current_app.config['DISABLED_ACCOUNT_MAILADDRESS_TEMPLATE'] % form.user.uid
        form.user.change_email(mail)

        current_app.user_backend.update(form.user, as_admin=True)

        flash(u'Passwort auf ein zufälliges und Mailadresse auf %s '
              u'gesetzt.' % mail, 'success')

        current_app.mail_backend.send(
            current_app.config['MAIL_REGISTER_NOTIFY'],
            'mail/disable_notify.txt',
            username=form.user.uid, mail=oldmail, admin=current_user.uid)

        return redirect(url_for('admin.index'))

    return {'form': form}
