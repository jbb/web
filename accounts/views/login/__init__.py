# -*- coding: utf-8 -*-
from __future__ import absolute_import

from flask import Blueprint
from flask import current_app, redirect, request, g, flash, render_template, url_for
from flask.ext.login import login_user, logout_user, current_user
from urlparse import urljoin, urlparse

from .forms import LoginForm


bp = Blueprint('login', __name__)


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    print(target)
    print(test_url)
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc and \
           test_url.path == target


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated():
        return redirect(url_for('default.index'))

    form = LoginForm(request.form)
    if form.validate_on_submit():
        try:
            user = current_app.user_backend.auth(form.username.data,
                                                 form.password.data)
            login_user(user)
            flash(u'Erfolgreich eingeloggt', 'success')

            next = request.form['next']
            if not is_safe_url(next):
                next = None
            return redirect(next or url_for('default.index'))
        except (current_app.user_backend.NoSuchUserError,
                current_app.user_backend.InvalidPasswordError):
            flash(u'Ungültiger Benutzername und/oder Passwort', 'error')

    return render_template("login/login.html", form=form,
                           next=request.values.get('next'))


@bp.route('/logout')
def logout():
    logout_user()
    flash(u'Erfolgreich ausgeloggt.', 'success')
    return redirect(url_for('.login'))
