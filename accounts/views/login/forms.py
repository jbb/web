# -*- coding: utf-8 -*-
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField, validators


class LoginForm(Form):
    username = TextField(u'Benutzername')
    password = PasswordField('Passwort', [validators.Required()])
