# -*- coding: utf-8 -*-
from flask import current_app
from itsdangerous import BadSignature, SignatureExpired, URLSafeTimedSerializer
from werkzeug.exceptions import Forbidden


class Confirmation(URLSafeTimedSerializer):

    def __init__(self, realm, key=None, **kwargs):
        if key is None:
            key = current_app.config['SECRET_KEY']
        super(Confirmation, self).__init__(key, salt=realm, **kwargs)

    def loads_http(self, s, max_age=None, return_timestamp=False, salt=None):
        """
        Like `Confirmation.loads`, but raise HTTP exceptions with appropriate
        messages instead of `BadSignature` or `SignatureExpired`.
        """

        try:
            return self.loads(s, max_age, return_timestamp, salt)
        except BadSignature:
            raise Forbidden(u'Ungültiger Bestätigungslink.')
        except SignatureExpired:
            raise Forbidden(u'Bestätigungslink ist zu alt.')
