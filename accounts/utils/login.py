# -*- coding: utf-8 -*-
from flask import current_app
from flask.ext.login import LoginManager, current_user
from functools import wraps
from werkzeug.exceptions import Forbidden
from itsdangerous import base64_decode, base64_encode, compact_json


def create_login_manager():
    login_manager = LoginManager()
    login_manager.login_message = 'Bitte einloggen'
    login_manager.login_view = 'login.login'

    @login_manager.user_loader
    def load_user(user_id):
        try:
            username, password = parse_userid(user_id)
            return current_app.user_backend.auth(username, password)
        except (current_app.user_backend.NoSuchUserError,
                current_app.user_backend.InvalidPasswordError):
            return None

    return login_manager


def create_userid(username, password):
    userid = (username, password)
    return base64_encode(compact_json.dumps(userid))


def parse_userid(value):
    return compact_json.loads(base64_decode(value))


def logout_required(f):
    @wraps(f)
    def logout_required_(*args, **kwargs):
        if current_user.is_authenticated():
            raise Forbidden(u'Diese Seite ist nur für nicht eingeloggte Benutzer gedacht!')
        return f(*args, **kwargs)
    return logout_required_

