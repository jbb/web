# -*- coding: utf-8 -*-
from flask.ext import script


class Command(script.Command):
    """
    This changes the Command class from Flask-Script in such way,
    that url_for will generate correct urls.
    """

    def __call__(self, app=None, *args, **kwargs):
        base_url = '%s://%s%s' % (
            app.config.get('PREFERRED_URL_SCHEME') or 'http',
            app.config.get('SERVER_NAME') or 'localhost',
            app.config.get('APPLICATION_ROOT') or '/')

        with app.test_request_context(base_url=base_url):
            return self.run(*args, **kwargs)


class TablePrinter(object):

    def __init__(self, headers=None, separator='|'):
        self.headers = headers
        self.separator = separator

        self.format_string = ''
        self.widths = list()

        if headers is not None:
            self._calulate_widths([headers])

    def _calulate_widths(self, rows):
        def _get_column_count(rows):
            return min([len(row) for row in rows])

        def _column_width(column, width):
            widths = [len(str(elem)) for elem in column]
            widths.append(width)
            return max(widths)

        columns = _get_column_count(rows)
        while len(self.widths) < columns:
            self.widths.append(0)

        self.widths = [_column_width(column, width)
                       for column, width
                       in zip(zip(*rows), self.widths)]
        self._update_format_string()


    def _update_format_string(self):
        sep = ' %s ' % self.separator
        self.format_string = '%s %s %s' % (
            self.separator,
            sep.join(['%%-%ds' % width for width in self.widths]),
            self.separator)

    def output(self, rows):
        if len(rows) > 0:
            self._calulate_widths(rows)

        if self.headers is not None:
            self._print_row(self.headers)
            self._print_headline()

        for row in rows:
            self._print_row(row)

    def _print_headline(self):
        print('%s%s%s' % (
            self.separator,
            self.separator.join(['-' * (width + 2) for width in self.widths]),
            self.separator))

    def _print_row(self, row):
        print(self.format_string % tuple(row))


class ConsoleForm(object):
    _ready = False

    def __init__(self, formcls, **kwargs):
        self.form = formcls()
        self._fill(kwargs)
        self._ready = True

    def _fill(self, data):
        for key, value in data.items():
            field = getattr(self.form, key, None)
            if field is not None:
                field.data = value

    def print_errors(self):
        for field, errors in self.form.errors.items():
            if len(errors) > 1:
                print('%s:' % field)
                for error in errors:
                    print('    %s' % error)
            else:
                print('%s: %s' % (field, errors[0]))

    def __getattr__(self, name):
        return getattr(self.form, name)

    def __setattr__(self, name, value):
        if self._ready and name not in self.__dict__:
            setattr(self.form, name, value)
        else:
            super(ConsoleForm, self).__setattr__(name, value)

    def __delattr__(self, name):
        if self._ready and name not in self.__dict__:
            delattr(self.form, name)
        else:
            super(ConsoleForm, self).__delattr__(name)
