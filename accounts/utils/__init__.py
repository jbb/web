# -*- coding: utf-8 -*-
import importlib
from functools import wraps
from flask import render_template, request
from wtforms.validators import Regexp, ValidationError


# using http://flask.pocoo.org/docs/patterns/viewdecorators/
def templated(template=None):
    def templated_(f):
        @wraps(f)
        def templated__(*args, **kwargs):
            template_name = template
            if template_name is None:
                template_name = request.endpoint \
                    .replace('.', '/') + '.html'
            ctx = f(*args, **kwargs)
            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx
            return render_template(template_name, **ctx)
        return templated__
    return templated_


def ensure_utf8(s):
    if isinstance(s, unicode):
        s = s.encode('utf8')
    return s


class NotRegexp(Regexp):
    """
    Like wtforms.validators.Regexp, but rejects data that DOES match the regex.
    """
    def __call__(self, form, field):
        if self.regex.match(field.data or u''):
            if self.message is None:
                self.message = field.gettext(u'Invalid input.')

            raise ValidationError(self.message)


def get_backend(path, app):
    module = path.rsplit(".", 1).pop()
    class_name = '%sBackend' % module.title()
    backend_class = getattr(importlib.import_module(path), class_name)
    return backend_class(app)
