# -*- coding: utf-8 -*-
import re
from flask import current_app, Markup, url_for
from flask.ext.wtf import Form
from flask.ext.login import current_user
from wtforms import TextField, PasswordField, ValidationError, BooleanField,\
                    validators
from wtforms.form import FormMeta
from utils import NotRegexp


USERNAME_RE = re.compile(r'^[a-zA-Z][a-zA-Z0-9-]{1,15}$')
USERNAME_EXCLUDE_RE = re.compile(r'^(admin|root)')


class RegisterForm(Form):
    username = TextField('Benutzername', [
        validators.Regexp(USERNAME_RE, message=u'Benutzername darf nur aus a-z, '
                          u'Zahlen und - bestehen (2-16 Zeichen, am Anfang nur a-z).'),
        NotRegexp(USERNAME_EXCLUDE_RE, message=u'Dieser Benutzername ist nicht erlaubt.'),
    ])
    mail = TextField('E-Mail-Adresse', [validators.Email(), validators.Length(min=6, max=50)])
    question = TextField('Hauptstadt von Deutschland?', [validators.AnyOf(
        ('Berlin', 'berlin'), message=u'Bitte beantworte die Frage.')])

    def validate_username(self, field):
        try:
            current_app.user_backend.get_by_uid(field.data)
        except current_app.user_backend.NoSuchUserError:
            if current_app.username_blacklist:
                if field.data.lower() in current_app.username_blacklist:
                    raise ValidationError(Markup(
                        u'Dieser Benutzername ist momentan nicht erlaubt. '
                        u'<a href="%s">Weitere Informationen</a>'
                        % url_for('default.about')))
        else:
            raise ValidationError(u'Dieser Benutzername ist schon vergeben.')

    def validate_mail(self, field):
        try:
            current_app.user_backend.get_by_mail(field.data)
        except current_app.user_backend.NoSuchUserError:
            pass
        else:
            raise ValidationError(Markup(
                u'Ein Benutzername mit dieser Adresse existiert bereits. '
                u'Falls du deinen Benutzernamen vergessen hast, kannst du '
                u'die <a href="%s">Passwort-vergessen-Funktion</a> benutzen.'
                % url_for('default.lost_password')))

class AdminCreateAccountForm(RegisterForm):
    def validate_username(self, field):
        try:
            current_app.user_backend.get_by_uid(field.data)
        except current_app.user_backend.NoSuchUserError:
            return
        else:
            raise ValidationError(u'Dieser Benutzername ist schon vergeben')

    question = None


class RegisterCompleteForm(Form):
    password = PasswordField('Passwort', [validators.Required(),
                                          validators.EqualTo('password_confirm', message=u'Passwörter stimmen nicht überein')])
    password_confirm = PasswordField(u'Passwort bestätigen')
    # n.b. this form is also used in lost_password_complete


class LostPasswordForm(Form):
    username_or_mail = TextField(u'Benutzername oder E-Mail')
    user = None

    def validate_username_or_mail(self, field):
        if '@' not in field.data:
            try:
                self.user = current_app.user_backend.get_by_uid(field.data)
            except current_app.user_backend.NoSuchUserError:
                raise ValidationError(u'Es gibt keinen Benutzer mit diesem Namen.')
        else:
            try:
                self.user = current_app.user_backend.get_by_mail(field.data)
            except current_app.user_backend.NoSuchUserError:
                raise ValidationError(u'Es gibt keinen Benutzer mit dieser Adresse.')


class SettingsMeta(FormMeta):
    def __call__(cls, *args, **kwargs):
        for service in current_app.all_services:
            setattr(cls, 'password_%s' % service.id, PasswordField(
                u'Passwort für %s' % service.name, [
                    validators.Optional(),
                    validators.EqualTo(
                        'password_confirm_%s' % service.id,
                        message=u'Passwörter stimmen nicht überein'),
                ]))
            setattr(cls, 'password_confirm_%s' % service.id, PasswordField(
                u'Passwort für %s (Bestätigung)' % service.name))
            setattr(cls, 'delete_%s' % service.id, BooleanField(
                u'Passwort für %s löschen' % service.name))

        return super(SettingsMeta, cls).__call__(*args, **kwargs)


class SettingsForm(Form):
    __metaclass__ = SettingsMeta

    old_password = PasswordField('Altes Passwort')
    password = PasswordField('Neues Passwort', [validators.Optional(),
                                                validators.EqualTo('password_confirm', message=u'Passwörter stimmen nicht überein')])
    password_confirm = PasswordField(u'Passwort bestätigen')
    mail = TextField('E-Mail-Adresse', [validators.Optional(), validators.Email(), validators.Length(min=6, max=50)])

    def validate_old_password(self, field):
        if self.password.data:
            if not field.data:
                raise ValidationError(u'Gib bitte dein altes Passwort ein, um ein neues zu setzen.')
            if field.data != current_user.password:
                raise ValidationError(u'Altes Passwort ist falsch.')

    def validate_mail(self, field):
        results = current_app.user_backend.find_by_mail(field.data)
        for user in results:
            if user.uid != current_user.uid:
                raise ValidationError(u'Diese E-Mail-Adresse wird schon von einem anderen Account benutzt!')

    def get_servicepassword(self, service_id):
        return getattr(self, 'password_%s' % service_id)

    def get_servicepasswordconfirm(self, service_id):
        return getattr(self, 'password_confirm_%s' % service_id)

    def get_servicedelete(self, service_id):
        return getattr(self, 'delete_%s' % service_id)


class AdminDisableAccountForm(Form):
    username = TextField(u'Benutzername')
    user = None

    def validate_username(self, field):
        try:
            self.user = current_app.user_backend.get_by_uid(field.data)
        except current_app.user_backend.NoSuchUserError:
            raise ValidationError(u'Dieser Benutzername existiert nicht')
