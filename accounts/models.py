# -*- coding: utf-8 -*-
from __future__ import absolute_import
from flask.ext.login import UserMixin

from accounts.utils.login import create_userid


class Account(UserMixin):
    """
    An Account represents a complex ldap tree entry for spline users.
    For each service a spline user can have a different password.
    """
    _ready = False

    def __init__(self, uid, mail, services=None, password=None, uidNumber=None):
        self.uid = uid.encode('utf8') if isinstance(uid, unicode) else uid
        self.services = list() if services is None else services
        self.password = password.encode('utf8') if isinstance(password, unicode) else password
        self.new_password_root = None
        self.new_password_services = {}
        self.attributes = {}
        self.uidNumber = uidNumber

        self._set_attribute('mail', mail)
        self._ready = True

    def __repr__(self):
        return "<Account uid=%s>" % self.uid

    def reset_password(self, service):
        self.new_password_services[service] = (None, None)

    def change_password(self, new_password, old_password='', service=None):
        """
        Changes a password for a given service. You have to use the
        UserBackend to make the changes permanent. If no service is given,
        the root password will be changed.
        """

        if isinstance(new_password, unicode):
            new_password = new_password.encode('utf8')

        if isinstance(old_password, unicode):
            old_password = old_password.encode('utf8')

        if not service:
            self.new_password_root = (old_password, new_password)
        else:
            self.new_password_services[service] = (old_password, new_password)

    def _set_attribute(self, key, value):
        if isinstance(value, unicode):
            value = value.encode('utf8')

        self.attributes[key] = value

    def change_email(self, new_mail):
        """
        Changes the mail address of an account. You have to use the
        AccountService class to make changes permanent.
        """
        self._set_attribute('mail', new_mail)

    def __getattr__(self, name):
        if name in self.attributes:
            return self.attributes[name]

        raise AttributeError("'%s' object has no attribute '%s'" %
                             (self.__class__.__name__, name))

    def __setattr__(self, name, value):
        if self._ready and name not in self.__dict__:
            self._set_attribute(name, value)
        else:
            super(Account, self).__setattr__(name, value)

    def get_id(self):
        """
        This is for flask-login. The returned string is saved inside
        the cookie and used to identify the user.
        """
        return create_userid(self.uid, self.password)


class Service(object):
    def __init__(self, service_id, name, url):
        self.id = service_id
        self.name = name
        self.url = url

        #: Wether the user has a separate password for this service.
        self.changed = False

    def __repr__(self):
        return '<Service %s>' % self.id
